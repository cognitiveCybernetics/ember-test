import Ember from 'ember';
import config from './config/environment';



var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {	
	this.resource( 'index', { path: '/' } );
	this.resource('first-route',{path: 'first-route'});
	this.resource('databinding',{path: 'databinding'});
	this.resource('second-route',{path: 'second-route'});
	this.resource('team-route',{path: 'team-route'});
	this.resource('people',{path: '/people'});
	this.resource('person',{path: '/people/:id'});
});



export default Router;
