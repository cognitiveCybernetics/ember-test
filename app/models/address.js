import DS from 'ember-data';

var address = DS.Model.extend({ 
  address:      DS.attr('string'), 
  category:       DS.attr('string'),
  person:     DS.belongsTo('person')
});
address.reopenClass({
    FIXTURES: [
        {
            id: 1,
            address: "MBP-F2-BLR",
            category: 'office',
            person: 1
        },
        {
        	 id: 2,
             address: "SOME_ADDRESS_1",
             category: 'home',
             person: 1            	 
        },
        {
       	 id: 3,
            address: "SOME_ADDRESS_2",
            category: 'home',
            person: 2
       },
       {
      	 id: 4,
           address: "SOME_ADDRESS_3",
           category: 'home',
           person: 3
      },     
      {
          id: 5,
          address: "BTP-BLR",
          category: 'office',
          person: 3
      },
      {
          id: 6,
          address: "KOL-GTP",
          category: 'office',
          person: 2
      }
    ]
});
export default address;