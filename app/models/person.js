import DS from 'ember-data';

var person = DS.Model.extend({ 
  firstName:      DS.attr('string'), 
  lastName:       DS.attr('string'),  
  phone:          DS.attr('string'),  
  email:          DS.attr('string'),
  addresses:      DS.hasMany('address',{async: true})
});

person.reopenClass({
    FIXTURES: [
        {
            id: 1,
            firstName: "ABC",
            lastName: 'abc',
            phone:'123123',
            email:'abc@gmail.com',
            addresses : [1,2]
        },
        {
            id: 2,
            firstName: "DEF",
            lastName: 'def',
            phone:'122233',
            email:'def@gmail.com',
            addresses : [6,3]
        },
        {
            id: 3,
            firstName: "XYZ",
            lastName: 'xyz',
            phone:'333333',
            email:'xyz@gmail.com',
            addresses : [5,4]
        }
    ]
});
export default person;