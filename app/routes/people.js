import Ember from 'ember';

 var PeopleRoute=Ember.Route.extend({
	
	model: function(){
		return this.store.find('person');
	}
});
 
 export default PeopleRoute;